import os
import json
import datetime
__location__ = os.path.realpath(os.path.join(os.getcwd(), os.path.dirname(__file__)))


class UnifiedMapping:
    def __init__(self) -> None:
        pass

    def read_json_file(self, filename):
        # read file
        with open(os.path.join(__location__, f"{filename}"), "r") as filetoread:
            data = filetoread.read()

        # parse file
        content = json.loads(data)

        return content

    def map_salesforce_address(
        self, addresses, address_mapping, payload, endpoint="contact"
    ):
        if isinstance(addresses, list):
            other_address_mapping = {}
            if len(addresses) > 0:
                for key in address_mapping.keys():
                    if key in addresses[0]:
                        if addresses[0][key]:
                            payload[address_mapping[key]] = addresses[0][key]

                    if len(addresses) > 1:
                        keyother = address_mapping[key].replace("Mailing", "Other")
                        if endpoint == "account":
                            keyother = address_mapping[key].replace(
                                "Billing", "Shipping"
                            )
                        if addresses[1][key]:
                            payload[keyother] = addresses[1][key]

        return payload

    # Microsoft dynamics address mapping
    def map_dynamics_address(self, address, address_mapping, payload):
        if isinstance(address, dict):
            for key, value in address.items():
                if key in address_mapping.keys():
                    payload[address_mapping[key]] = value
        return payload

    def map_dynamics_lineItems(self, line_items, line_items_mapping):
        if isinstance(line_items, str):
            line_items = json.loads(line_items)
        if isinstance(line_items, list):
            if len(line_items) > 0:
                line_items_ = []
                for item in line_items:
                    item_ = {}
                    for key, value in item.items():
                        if key in line_items_mapping[0].keys():
                            item_[line_items_mapping[0][key]] = value
                    payload_return = {}
                    for key in item_.keys():
                        if key is not None:
                            payload_return[key] = item_[key]
                    line_items_ += [payload_return]

        return line_items_

    def map_custom_fields(self, payload, fields):
        # Populate custom fields.
        for key, val in fields:
            payload[key] = val
        return payload

    def map_customer_ref(self,payload,mapping,record):
        for lookup_key in mapping.keys():
            
            if lookup_key=='addresses':
                val = record.get(lookup_key, "")[0]
                #Endpoint not accepting billing address
                # payload['billingPostalAddress'] = {
                #     "city":val['city'],
                #     "street":val['line1'],
                #     "state":val['state'],
                #     "countryLetterCode":val['country'],
                #     "postalCode":val['postalCode'],
                # }
            else:
                val = record.get(lookup_key, "")
                if val:
                    payload[mapping[lookup_key]] = val
                
        return payload        


    def prepare_payload(self, record, endpoint="invoice", target="dynamics-bc",ignore=True):
        mapping = self.read_json_file(f"mapping_{target}.json")
        ignore = mapping["ignore"]
        mapping = mapping[endpoint]
        payload = {}
        payload_return = {}
        lookup_keys = mapping.keys()
        line_items = []
        for lookup_key in lookup_keys:
            if lookup_key == "address" and target == "dynamics-bc":
                payload = self.map_dynamics_address(
                    record.get(lookup_key, []), mapping[lookup_key], payload
                )
            elif lookup_key == "lineItems" and target == "dynamics-bc":
                line_items = self.map_dynamics_lineItems(
                    record.get(lookup_key, []), mapping[lookup_key]
                )
            elif "date" in lookup_key.lower():
                val = record.get(lookup_key)
                if isinstance(val, datetime.datetime):
                    val = val.isoformat()
                if val:
                    payload[mapping[lookup_key]] = val.split("T")[0]
                else:
                    val = ""
            elif lookup_key=='customerRef':
                payload = self.map_customer_ref(payload,mapping['customerRef'],record['customerRef'])      
            else:
                val = record.get(lookup_key, "")
                if val:
                    payload[mapping[lookup_key]] = val
        if ignore is True:
            for key in payload.keys():
                if key not in ignore and key is not None:
                    payload_return[key] = payload[key]
        else:
            for key in payload.keys():
                payload_return[key] = payload[key]

        if None in payload_return.keys():
            payload_return.pop(None)

        return payload_return, line_items
