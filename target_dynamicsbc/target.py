"""DynamicsBC target class."""

from __future__ import annotations

from singer_sdk import typing as th
from singer_sdk.target_base import Target

from target_dynamicsbc.sinks import DynamicsSink


class TargetDynamicsBC(Target):
    """Sample target for DynamicsBC."""

    name = "target-dynamicsbc"
    config_jsonschema = th.PropertiesList(
        th.Property(
            "access_token",
            th.StringType,
            required=False,
        ),
        th.Property(
            "refresh_token",
            th.StringType,
            required=True,
        ),
        th.Property(
            "client_secret",
            th.StringType,
            required=True,
        ),
        th.Property(
            "client_id",
            th.StringType,
            required=True,
        ),
        th.Property(
            "start_date",
            th.DateTimeType,
            required=True,
            description="The earliest record date to sync",
        ),
        th.Property(
            "environment_name",
            th.StringType,
            required=True,
        ),
        th.Property(
            "company_name",
            th.StringType,
            required=True,
        ),
    ).to_dict()

    default_sink_class = DynamicsSink


if __name__ == "__main__":
    TargetDynamicsBC.cli()
