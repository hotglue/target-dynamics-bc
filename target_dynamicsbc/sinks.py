"""Dynamics target sink class, which handles writing streams."""

import json
from datetime import datetime, timedelta
from typing import Dict, List, Optional

import requests
import singer
from singer_sdk.plugin_base import PluginBase
from singer_sdk.sinks import RecordSink

from target_dynamicsbc.mapping import UnifiedMapping

LOGGER = singer.get_logger()


class DynamicsAuth(requests.auth.AuthBase):
    def __init__(self, config):
        self.__config = config
        self.__client_id = config["client_id"]
        self.__client_secret = config["client_secret"]
        self.__redirect_uri = config.get(
            "redirect_uri", "https://qa.hotglue.xyz/callback"
        )
        self.__refresh_token = config["refresh_token"]

        self.__session = requests.Session()
        self.__access_token = None
        self.__expires_at = None

    def ensure_access_token(self):
        if self.__access_token is None or self.__expires_at <= datetime.utcnow():
            response = self.__session.post(
                "https://login.microsoftonline.com/common/oauth2/token",
                data={
                    "client_id": self.__client_id,
                    "client_secret": self.__client_secret,
                    "redirect_uri": self.__redirect_uri,
                    "refresh_token": self.__refresh_token,
                    "grant_type": "refresh_token",
                },
            )

            if response.status_code != 200:
                raise Exception(response.text)

            data = response.json()

            self.__access_token = data["access_token"]
            self.__config["refresh_token"] = data["refresh_token"]
            self.__config["expires_in"] = data["expires_in"]
            self.__config["access_token"] = data["access_token"]

            with open("config.json", "w") as outfile:
                json.dump(self.__config, outfile, indent=4)

            self.__expires_at = datetime.utcnow() + timedelta(
                seconds=int(data["expires_in"]) - 10
            )  # pad by 10 seconds for clock drift

    def __call__(self, r):
        self.ensure_access_token()
        r.headers["Authorization"] = "Bearer {}".format(self.__access_token)
        return r


class DynamicsSink(RecordSink):
    """Dynamics target sink class."""

    def __init__(
        self,
        target: PluginBase,
        stream_name: str,
        schema: Dict,
        key_properties: Optional[List[str]],
    ) -> None:
        super().__init__(target, stream_name, schema, key_properties)
        # Save config for refresh_token saving
        self.config_file = target.config
        self.target_name = "dynamics-bc"
        self.url = f"https://api.businesscentral.dynamics.com/v2.0/{self.config['environment_name']}/api/v2.0/"
        self.company_id = self.get_company_id(self.config['company_name'])
        self.customer_id = self.get_entity_ids("customers")
        self.products_id = self.get_entity_ids("items")
        self.vendors_id = self.get_entity_ids("vendors")

    def get_auth(self):
        auth = DynamicsAuth(dict(self.config))
        r = requests.Session()
        auth = auth(r)
        return auth

    def get_company_id(self,company_name):

        url = f"https://api.businesscentral.dynamics.com/v2.0/{self.config['environment_name']}/api/v2.0/companies"
        auth = self.get_auth()
        auth.headers.update({"Content-Type": "application/json"})
        res = auth.get(url)

        if res.status_code != 200:
            LOGGER.info(f"REQUES STATUS CODE: {res.status_code}")
            raise Exception(res.text)
        else:
            LOGGER.info("Quering companies: \n")
            LOGGER.info(res.text)

            for company in res.json()['value']:

                if company['name'] == company_name:
                    return company['id']

            raise Exception(f"COMPANY \"{company_name}\" NOT FOUND.")

    def get_entity_ids(self,entity = "customers"):

        url = f"https://api.businesscentral.dynamics.com/v2.0/{self.config['environment_name']}/api/v2.0/companies"
        url += f"({self.company_id})/{entity}"

        offset = 0 
        ids = {}

        while offset is not None: 

            auth = self.get_auth()
            auth.headers.update({"Content-Type": "application/json"})
            res = auth.get(url,params={"$skip":offset})

            if res.status_code != 200:
                LOGGER.info(f"REQUES STATUS CODE: {res.status_code}")
                raise Exception(res.text)
            else:
                # LOGGER.info("Quering companies: \n")
                LOGGER.info(res.text)
                result = res.json()['value']
                for customer in result:
                    ids.update({
                        customer['number']:customer['id'],
                        customer['displayName']:customer['id'],
                        })
                
                if result: 
                    offset += len(result)
                else: 
                    offset = None

        return ids

    def invoice_product_hanlder(self,invoice_line):
        if invoice_line.get('itemId'):
            item_id = self.products_id.get(invoice_line.get('itemId'))
        else:
            item_id = self.products_id.get(invoice_line.get("itemName"))
        if not item_id:
            raise Exception(f"Could not find item_id for Invoice Line:\n{invoice_line}")
        invoice_line.update({"itemId":item_id})
        invoice_line.pop("itemName")
        return invoice_line

    def invoices_upload(self, record):
        mapping = UnifiedMapping()
        payload_invoice, payload_invoice_lines = mapping.prepare_payload(
            record, "invoices", self.target_name
        )
        
        # Handling customer_number -> customer_id
        customer_id = self.customer_id.get(payload_invoice['customerId'])
        if not customer_id:
            raise Exception(f"Could not find customer_id for CustomerNumber:{payload_invoice['customerId']}")
        payload_invoice.update({"customerId":customer_id})

        for invoice_line in payload_invoice_lines:
            invoice_line = self.invoice_product_hanlder(invoice_line)

        auth = self.get_auth()
        url = self.url
        url = f"{url}companies({self.company_id})/salesInvoices"
        auth.headers.update({"Content-Type": "application/json"})
        # Sending Invoice Payload
        res = auth.post(url, json=payload_invoice)

        if res.status_code != 201:
            LOGGER.info(f"REQUES STATUS CODE: {res.status_code}")
            raise Exception(res.text)
        else:
            LOGGER.info("New Invoice Created Successfully: \n")
            LOGGER.info(res.text)

            invoice_id = res.json().get("id")

            if invoice_id:
                for payload_line in payload_invoice_lines:
                    url = f"{self.url}companies({self.company_id})/purchaseInvoices"
                    url += f"({invoice_id})/salesInvoiceLines"
                    res = auth.post(url, json=payload_line)

                    if res.status_code != 201:
                        LOGGER.info(f"REQUES STATUS CODE: {res.status_code}")
                        raise Exception(res.text)
                    else:
                        LOGGER.info(
                            f"New Invoice Line for Invoice Id:{invoice_id} Created Successfully: \n"
                        )
                        LOGGER.info(res.text)

    def purchase_invoices_upload(self,record):
        mapping = UnifiedMapping()
        payload_invoice, payload_invoice_lines = mapping.prepare_payload(
            record, "purchase_invoices", self.target_name
        )
        # Handling vendor_name -> vendor_id
        vendor_number = payload_invoice.get("vendorNumber")
        vendor_name = payload_invoice.get("vendorName")
        #Check if there was no supplierName in payload but vendorName
        if vendor_name is None and record.get('vendorName'):
            vendor_name = record.get('vendorName')
            payload_invoice['vendorName'] = vendor_name
        if not vendor_number:
            vendor_id = vendor_name
            vendor_id = self.vendors_id.get(vendor_id)
            if not vendor_id:
                raise Exception(f"Vendor Id not for {vendor_name}")
            payload_invoice['vendorId'] = vendor_id
            payload_invoice.pop("vendorName")

        for invoice_line in payload_invoice_lines:
            
            invoice_line = self.invoice_product_hanlder(invoice_line)

        auth = self.get_auth()
        url = self.url
        url = f"{url}companies({self.company_id})/purchaseInvoices"
        auth.headers.update({"Content-Type": "application/json"})
        # Sending Invoice Payload
        res = auth.post(url, json=payload_invoice)

        if res.status_code != 201:
            LOGGER.info(f"REQUEST STATUS CODE: {res.status_code}")
            raise Exception(res.text)
        else:
            LOGGER.info("New Purchase Invoice Created Successfully: \n")
            LOGGER.info(res.text)

            invoice_id = res.json().get("id")

            if invoice_id:
                for payload_line in payload_invoice_lines:
                    url = f"{self.url}companies({self.company_id})/purchaseInvoices"
                    url += f"({invoice_id})/purchaseInvoiceLines"
                    res = auth.post(url, json=payload_line)

                    if res.status_code != 201:
                        LOGGER.info(f"REQUES STATUS CODE: {res.status_code}")
                        raise Exception(res.text)
                    else:
                        LOGGER.info(
                            f"New Purchase Invoice Line for Invoice Id:{invoice_id} Created Successfully: \n"
                        )
                        LOGGER.info(res.text)

    def credit_notes_upload(self,record):
        mapping = UnifiedMapping()
        payload, payload_lines = mapping.prepare_payload(
            record, "credit_memos", self.target_name,False
        )

        #Populate ItemId for Credit memo lines
        for invoice_line in payload_lines:
            invoice_line = self.invoice_product_hanlder(invoice_line)
        auth = self.get_auth()
        url = f"{self.url}companies({self.company_id})/salesCreditMemos"
        customer_id = self.customer_id.get(payload['customerId'])
        if not customer_id:
            print(f"Could not find customer_id for CustomerNumber:{payload['customerId']}. Skipping.")
        else:    
            payload['customerId'] = customer_id
            
            res = auth.post(url, json=payload)
            if res.status_code != 201:
                LOGGER.info(f"REQUES STATUS CODE: {res.status_code}")
                raise Exception(res.text)
            else:
                LOGGER.info(
                    f"New Credit Memo Created Successfully: \n"
                )
                LOGGER.info(res.text)
                memo_id = res.json().get("id")

            if memo_id:
                for payload_line in payload_lines:
                    url = f"{self.url}companies({self.company_id})/salesCreditMemos({memo_id})"
                    url += "/salesCreditMemoLines"
                    res = auth.post(url, json=payload_line)

                    if res.status_code != 201:
                        LOGGER.info(f"REQUES STATUS CODE: {res.status_code}")
                        raise Exception(res.text)
                    else:
                        LOGGER.info(
                            f"New Line For Credit Memo Id:{memo_id} Created Successfully: \n"
                        )
                        LOGGER.info(res.text)

    def vendors_upload(self,record):

        #Lets use new format of mapping
        payload = {
            "displayName":record.get("vendorName"),
            "email":record.get("emailAddress"),
            "currencyCode":record.get("currency"),
        }
        if "addresses" in record:
            if len(record["addresses"])>0:
                address = record["addresses"][0]
                if "line1" in address:
                    payload["addressLine1"] = address['line1']
                if "line2" in address:
                    payload["addressLine2"] = address['line2']
                if "city" in address:
                    payload["city"] = address['city']
                if "state" in address:
                    payload["state"] = address['state']
                if "postalCode" in address:
                    payload["postalCode"] = address['postalCode']
                if "country" in address:
                    payload["country"] = address['country']

        if "phoneNumbers" in record:
            if len(record['phoneNumbers'])>0:
                if "number" in record['phoneNumbers'][0]:
                    payload['phoneNumber'] = record['phoneNumbers'][0]['number']

        auth = self.get_auth()
        url = f"{self.url}companies({self.company_id})/vendors"
            
        res = auth.post(url, json=payload)
        if res.status_code != 201:
            LOGGER.info(f"REQUES STATUS CODE: {res.status_code}")
            raise Exception(res.text)
        else:
            LOGGER.info(
                f"New Vendor Created Successfully: \n"
            )
            LOGGER.info(res.text)
        
    def process_record(self, record: dict, context: dict) -> None:
        """Process the record."""
        if self.stream_name == "Invoices":
            self.invoices_upload(record)
        if self.stream_name == "PurchaseInvoices" or "Bills":
            self.purchase_invoices_upload(record)
        if self.stream_name == "CreditNotes":
            self.credit_notes_upload(record)
        if self.stream_name == "Vendors":
            self.vendors_upload(record)
